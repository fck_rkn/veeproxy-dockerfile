#!/usr/bin/env bash

export CURRENT_DIR=$PWD
mkdir /tmp/build-veeproxy
mkdir /tmp/build-veeproxy/src
cd /tmp/build-veeproxy/src

export GO111MODULE=on # Enable go module
export GOPATH=/tmp/build-veeproxy

git clone https://github.com/VeeSecurity/SOCKS5Engine/
find /tmp/build-veeproxy/src -type f -exec sed -i 's/VeeProxyEngine\/socks5/github.com\/VeeSecurity\/SOCKS5Engine\/socks5/gI' {} \;

cd /tmp/build-veeproxy/src/SOCKS5Engine
go build -o $CURRENT_DIR/proxy /tmp/build-veeproxy/src/SOCKS5Engine

rm -Rf /tmp/build-veeproxy
