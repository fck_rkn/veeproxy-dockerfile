FROM golang:latest
ENV GO111MODULE=on

WORKDIR /root
RUN git clone https://github.com/VeeSecurity/SOCKS5Engine && \
 find ./ -type f -exec sed -i 's/VeeProxyEngine\/socks5/github.com\/VeeSecurity\/SOCKS5Engine\/socks5/gI' {} \; && \
 cd /root/SOCKS5Engine && go build -o /root/proxy

FROM alpine:latest
RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=0 /root/proxy /root/
COPY ./VeeProxyEngine.conf /etc/
RUN mkdir /lib64 && ln -s /lib/libc.musl-x86_64.so.1 /lib64/ld-linux-x86-64.so.2
CMD ["./proxy"]
